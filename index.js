var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const path = require('path');
var bodyParser = require("body-parser");
var port = process.env.PORT || 8080;

let users = [{
  username: 'Jens',
  password: 'srilanka'
},
{
  username: 'Robin',
  password: 'srilanka',
}];

app.use(express.static(path.join(__dirname, 'public')));
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req, res){
  res.render('login',{});
});

app.post('/authenticate', function(req,res){
  let user = findUser(req.body.username);
  console.log(user);
  if (user && user.password === req.body.password) {
    res.render('chat',{});
  }
  else {
    console.log('permission failed');
    res.status(403).render('403',{});
  }
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});

findUser = (username) => {
  console.log(username);
  return users.find(user => {
    return user.username === username
  });
}
